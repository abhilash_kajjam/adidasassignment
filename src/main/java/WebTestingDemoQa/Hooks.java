package WebTestingDemoQa;

import java.net.MalformedURLException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class Hooks {
	
	public  WebDriver driver;
	//Properties props = new Properties();
	ConfigFileReader config = new ConfigFileReader();
	
    public WebDriver openBrowser(String browser,String appName) throws MalformedURLException {
    	
    	if(browser.equalsIgnoreCase("chrome")||browser.equalsIgnoreCase("Google Chrome"))
    	{
    		ChromeOptions options = new ChromeOptions();
    		options.addArguments("user-data-dir="+ "C:\\Users\\ajay.soni\\AppData\\Local\\Google\\Chrome\\User Data");
    		options.addArguments("start-maximized");
    		options.addArguments("disable-infobars");
    		System.setProperty("webdriver.chrome.driver","D:\\BP_SalesforceAutomation\\com.infosys.selenium\\webdrivers\\chrome\\2.35\\chromedriver.exe");
    		driver = new ChromeDriver(options);
    		System.out.println("URL");
    		driver.get(config.getApplicationUrl(appName));
    		//driver.get("https://bpcolleagues--hrmdev.cs85.my.salesforce.com");

    		driver.manage().deleteAllCookies();
    	
    	}
    	
    	return driver;
    	
    }

   @Before
    public static String printScenarioName(Scenario scenario) {
    	
        System.out.println("Run into Before Hook: printScenarioName");
        System.out.println("Print Scenario name in Before Hook: " + scenario.getName());
    
        return scenario.getName();
    }
    
	/****************************************************************************************************************
	 * FUNCTION NAME: setDriver
	 * CREATED BY: Abhilash
	 * DATE CREATED: 04-Feb-2019
	 * DESCRIPTION: This Function sets browser instance during execution
	 * PRE_REQUISITES: NA
	 * PARAMETERS: 1. strBrowser : either Firefox, Chrome or IE (Place the chromedriver and IEdriver in the location mentioned below, relative to the project directory)
	 * 2. strBrowserBin : path of the browser
	 * RETURN TYPE: webDriver
	 * RETURN VALUE: TRUE/FALSE
	 * EXAMPLE:
	 * REVIEWED BY:
	 * DATE REVIEWED:
	 * MODIFICATION LOG:
	 * ID                            DATE                       INITIALS                   MODIFICATIONS
	 ****************************************************************************************************************/
   
	public WebDriver setDriver(String strBrowser,String appName)
	{
		
		if(strBrowser.equalsIgnoreCase("chrome")||strBrowser.equalsIgnoreCase("GoogleChrome") || strBrowser.equalsIgnoreCase("Google Chrome"))
		{
			DesiredCapabilities chromeCap =  DesiredCapabilities.chrome();
			chromeCap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			//return this.setChromeDriver(strBrowserBin,strBrowserProfile);
			return this.setChromeDriver(appName ,System.getProperty("user.dir")+config.getChromeDriverPath(), "C:\\Users\\ajay.soni\\AppData\\Local\\Google\\Chrome\\User Data");
			//return this.setChromeDriver("C:/Users/Monika06/com.infosys.selenium/com.infosys.selenium/com.infosys.selenium/webdrivers/chrome/2.35/chromedriver.exe","C:\\Users\\Monika06\\AppData\\Local\\Google\\Chrome\\User Data");
		
		}
		else if(strBrowser.equalsIgnoreCase("IE") || strBrowser.equalsIgnoreCase("InternetExplorer"))
		{
			//invoke IE driver
			DesiredCapabilities ieCap =  DesiredCapabilities.internetExplorer();
			ieCap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			return this.setIEDriver(appName,System.getProperty("user.dir")+config.getIEDriverPath(), "");
			
		}
		else if(strBrowser.equalsIgnoreCase("firefox"))
		{
			
			return this.setFirefoxDriver(System.getProperty("user.dir")+config.getFirefoxDriverPath(),"");
			//return this.setFirefoxDriver("C:/Users/Monika06/com.infosys.selenium/com.infosys.selenium/com.infosys.selenium/webdrivers/firefox/geckodriver.exe","");
		
		}
		else if(strBrowser.equalsIgnoreCase("incognitoChrome"))
		{
			
			return this.setIncognitoChromeDriver(appName ,System.getProperty("user.dir")+config.getChromeDriverPath(), "C:\\Users\\nidhi.tripathi\\AppData\\Local\\Google\\Chrome\\User Data");
			//return this.setFirefoxDriver("C:/Users/nidhi.tripathi/com.infosys.selenium/com.infosys.selenium/com.infosys.selenium/webdrivers/firefox/geckodriver.exe","");
		
		}
		return null;		
	}

	private WebDriver setFirefoxDriver(String strBrowserBin, String strBrowserProfile) {

		return null;
	}

	/****************************************************************************************************************
	 * FUNCTION NAME: setIEDriver
	 * CREATED BY: Abhilash
	 * DATE CREATED: 04-Feb-2019
	 * DESCRIPTION: This Function sets  IE browser instance during execution
	 * PRE_REQUISITES: NA
	 * PARAMETERS: 1. strBrowserBin : path of the browser 3. strBrowserProfile : path to browser profile
	 * RETURN TYPE: webDriver
	 * RETURN VALUE: TRUE/FALSE
	 * EXAMPLE:
	 * REVIEWED BY:
	 * DATE REVIEWED:
	 * MODIFICATION LOG:
	 * ID                            DATE                       INITIALS                   MODIFICATIONS
	 ****************************************************************************************************************/

	@SuppressWarnings("deprecation")
	private WebDriver setIEDriver(String appName,String strBrowserBin,String strBrowserProfile) {
		try {
			//ChromeOptions options = new ChromeOptions();
			
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
            capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
            capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
            capabilities.setCapability("requireWindowFocus", true);
            //capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://ngnt056.bpweb.bp.com/de/upload");
            //System.setProperty("webdriver.ie.driver","P:\\GUI_Automation\\IEDriverServer.exe");
            // driver = new InternetExplorerDriver(capabilities);

			
			
			//InternetExplorerOptions options = new InternetExplorerOptions();
			System.setProperty("webdriver.ie.driver", strBrowserBin);
            

			driver = new InternetExplorerDriver(capabilities);
			driver.manage().window().maximize();
			driver.get(config.getApplicationUrl(appName));
			return driver;
		}
		catch(Exception e)
		{
			System.out.println("Error occured while instantiating IE driver"+e.getStackTrace());
			return null;
		}
	}

	/****************************************************************************************************************
	 * FUNCTION NAME: setChromeDriver
	 * CREATED BY: Abhilash
	 * DATE CREATED: 04-Feb-2019
	 * DESCRIPTION: This Function sets  Chrome browser instance during execution
	 * PRE_REQUISITES: NA
	 * PARAMETERS: 1 strBrowserBin : path of the browser 2. strBrowserProfile : path to browser profile
	 * RETURN TYPE: webDriver
	 * RETURN VALUE: TRUE/FALSE
	 * EXAMPLE:
	 * REVIEWED BY:
	 * DATE REVIEWED:
	 * MODIFICATION LOG:
	 * ID                            DATE                       INITIALS                   MODIFICATIONS
	 ****************************************************************************************************************/

	private WebDriver setChromeDriver(String appName,String strBrowserBin,String strBrowserProfile)
	{
		ChromeOptions options = new ChromeOptions();
		System.out.println(strBrowserBin);
		//options.addArguments("--incognito");
		//strBrowserBin = "C:/Users/Monika06/com.infosys.selenium/com.infosys.selenium/com.infosys.selenium/webdrivers/chrome/2.35/chromedriver.exe";
		strBrowserProfile = "C:\\Users\\ajay.soni\\AppData\\Local\\Google\\Chrome\\User Data";
		
		options.addArguments("user-data-dir=" + strBrowserProfile);
		options.addArguments("start-maximized");
		options.addArguments("disable-infobars");
		//options.setExperimentalOption(name, value)
		System.setProperty("webdriver.chrome.driver", strBrowserBin);
		driver = new ChromeDriver(options);
		driver.get(config.getApplicationUrl(appName));
		//driver.get("https://bpcolleagues--hrmdev.cs85.my.salesforce.com");
		
		return driver;
	}
	
	
	
	/****************************************************************************************************************
	 * FUNCTION NAME: setIncognitoChromeDriver
	 * CREATED BY: Abhilash
	 * DATE CREATED: 04-Feb-2019
	 * DESCRIPTION: This Function sets  Chrome browser instance during execution
	 * PRE_REQUISITES: NA
	 * PARAMETERS: 1 strBrowserBin : path of the browser 2. strBrowserProfile : path to browser profile
	 * RETURN TYPE: webDriver
	 * RETURN VALUE: TRUE/FALSE
	 * EXAMPLE:
	 * REVIEWED BY:
	 * DATE REVIEWED:
	 * MODIFICATION LOG:
	 * ID                            DATE                       INITIALS                   MODIFICATIONS
	 ****************************************************************************************************************/

	private WebDriver setIncognitoChromeDriver(String appName,String strBrowserBin,String strBrowserProfile)
	{
		ChromeOptions options = new ChromeOptions();
		System.out.println(strBrowserBin);
		options.addArguments("--incognito");
		//strBrowserBin = "C:/Users/Monika06/com.infosys.selenium/com.infosys.selenium/com.infosys.selenium/webdrivers/chrome/2.35/chromedriver.exe";
		strBrowserProfile = "C:\\Users\\ajay.soni\\AppData\\Local\\Google\\Chrome\\User Data";
		
		options.addArguments("user-data-dir=" + strBrowserProfile);
		options.addArguments("start-maximized");
		options.addArguments("disable-infobars");
		//options.setExperimentalOption(name, value)
		System.setProperty("webdriver.chrome.driver", strBrowserBin);
		driver = new ChromeDriver(options);
		driver.get(config.getApplicationUrl(appName));
		//driver.get("https://bpcolleagues--hrmdev.cs85.my.salesforce.com");
		
		return driver;
	}

 
    
   
   /* public void embedScreenshot(Scenario scenario) {
    	
        if(scenario.isFailed()) {
            try {
            	
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
                
            } 
            catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
 
        }
        
        driver.quit();
 
    }*/
	
	
    @After
    public void embedScreenshot(Scenario scenario) {
    	
        if(scenario.isFailed()) {
            try {
            	
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
                
            } 
            catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
 
        }
        
        driver.quit();
 
    }
}
