package WebTestingDemoQa;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;



public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath= "PropertyFiles//salesforce.properties";

	public ConfigFileReader(){
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				
				properties.load(reader);
				reader.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	public String getDriverPath(){
		String driverPath = properties.getProperty("driverPath");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
	}
	
	public long getImplicitlyWait() {		
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file.");		
	}
	
	public String getApplicationUrl(String appName) {
		
		if(appName.equalsIgnoreCase("myHR")) {
			
			String url = properties.getProperty("myHR");
			
			return url;
			
		}else if(appName.equalsIgnoreCase("serviceConsole")) {
			
			String url = properties.getProperty("serviceConsole");
			
			return url;
			
		}else if(appName.equalsIgnoreCase("employeePortal")) {
			
			String url = properties.getProperty("employeePortal");
			
			return url;
			
		}else if(appName.equalsIgnoreCase("chatBot")) {
			
			String url = properties.getProperty("chatBot");
			
			return url;
		}

		else throw new RuntimeException("Application url not specified in the salesforce.properties file.");
	}
	
	public String getChromeDriverPath(){
		String driverPath = properties.getProperty("Chromedriver");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Chrome driverPath not specified in the salesforce.properties file.");		
	}
	
	public String getFirefoxDriverPath(){
		String driverPath = properties.getProperty("FirefoxDriver");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Firefox driverPath not specified in the salesforce.properties file.");		
	}
	
	public String getIEDriverPath(){
		String driverPath = properties.getProperty("IEDriver");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("IE Driver Path not specified in the salesforce.properties file.");		
	}
	
	public String getmyHrID(){
		String driverPath = properties.getProperty("myHrId");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("myHR Id not specified in the salesforce.properties file.");		
	}
	
	public String getmyHRPwd(){
		String driverPath = properties.getProperty("myHrPassword");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("myHR password not specified in the salesforce.properties file.");		
	}
	
	public String getUkEmployeeSunburyId(){
		String driverPath = properties.getProperty("UkEmployeeSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK employee not specified in the salesforce.properties file.");		
	}
	
	public String getUkEmployeeSunburyPwd(){
		String driverPath = properties.getProperty("UkEmployeeSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK employee password not specified in the salesforce.properties file.");		
	}
	
	public String getUkLineManagerSunburyId(){
		String driverPath = properties.getProperty("UkLineManagerSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Line Manager Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkLineManagerSunburyPwd(){
		String driverPath = properties.getProperty("UkLineManagerSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Line ManagerSunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUkBFHRSunburyId(){
		String driverPath = properties.getProperty("UkBFHRSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK BFHR Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkBFHRSunburyPwd(){
		String driverPath = properties.getProperty("UkBFHRSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK BFHR Sunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUkCOESunburyId(){
		String driverPath = properties.getProperty("UkCOESunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK COE Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkCOESunburyPwd(){
		String driverPath = properties.getProperty("UkCOESunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK COE Sunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceManagerSunburyId(){
		String driverPath = properties.getProperty("UkHRServiceManagerSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Manager Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceManagerSunburyPwd(){
		String driverPath = properties.getProperty("UkHRServiceManagerSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Manager Sunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceSpecialistSunburyId(){
		String driverPath = properties.getProperty("UkHRServiceSpecialistSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Specialist Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceSpecialistSunburyPwd(){
		String driverPath = properties.getProperty("UkHRServiceSpecialistSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Specialist Sunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceTeamLeadSunburyId(){
		String driverPath = properties.getProperty("UkHRServiceTeamLeadSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Team Lead Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUkHRServiceTeamLeadSunburyPwd(){
		String driverPath = properties.getProperty("UkHRServiceTeamLeadSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK HR Service Team Lead Sunbury password not specified in the salesforce.properties file.");		
	}

	public String getUKKnowledgeSpecialistSunburyId(){
		String driverPath = properties.getProperty("UKKnowledgeSpecialistSunbury");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Knowledge Specialist Sunbury not specified in the salesforce.properties file.");		
	}
	
	public String getUKKnowledgeSpecialistSunburyPwd(){
		String driverPath = properties.getProperty("UKKnowledgeSpecialistSunburyPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Knowledge Specialist Sunbury password not specified in the salesforce.properties file.");		
	}
	
	public String getUSAEmployeeHoustonId(){
		String driverPath = properties.getProperty("USAEmployeeHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Employee Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSAEmployeeHoustonPwd(){
		String driverPath = properties.getProperty("USAEmployeeHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Employee Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSALineManagerHoustonId(){
		String driverPath = properties.getProperty("USALineManagerHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Line Manager Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSALineManagerHoustonPwd(){
		String driverPath = properties.getProperty("USALineManagerHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Line Manager Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSABFHRHoustonId(){
		String driverPath = properties.getProperty("USABFHRHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA BFHR Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSABFHRHoustonPwd(){
		String driverPath = properties.getProperty("USABFHRHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA BFHR Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSACOEHoustonId(){
		String driverPath = properties.getProperty("USACOEHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA COE Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSACOEHoustonPwd(){
		String driverPath = properties.getProperty("USACOEHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA COE Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceManagerHoustonId(){
		String driverPath = properties.getProperty("USAHRServiceManagerHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Manager Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceManagerHoustonPwd(){
		String driverPath = properties.getProperty("USAHRServiceManagerHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Manager Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceSpecialistHoustonId(){
		String driverPath = properties.getProperty("USAHRServiceSpecialistHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Specialist Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceSpecialistHoustonPwd(){
		String driverPath = properties.getProperty("USAHRServiceSpecialistHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Specialist Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceTeamLeadHoustonId(){
		String driverPath = properties.getProperty("USAHRServiceTeamLeadHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Team Lead Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSAHRServiceTeamLeadHoustonPwd(){
		String driverPath = properties.getProperty("USAHRServiceTeamLeadHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA HR Service Team Lead Houston password not specified in the salesforce.properties file.");		
	}

	public String getUSAKnowledgeSpecialistHoustonId(){
		String driverPath = properties.getProperty("USAKnowledgeSpecialistHouston");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Knowledge Specialist Houston not specified in the salesforce.properties file.");		
	}
	
	public String getUSAKnowledgeSpecialistHoustonPwd(){
		String driverPath = properties.getProperty("USAKnowledgeSpecialistHoustonPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("USA Knowledge Specialist Houston password not specified in the salesforce.properties file.");		
	}
	
	public String getReportingSpecialistId(){
		String driverPath = properties.getProperty("ReportingSpecialist");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Reporting Specialist not specified in the salesforce.properties file.");		
	}
	
	public String getReportingSpecialistPwd(){
		String driverPath = properties.getProperty("ReportingSpecialistPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Reporting Specialist password not specified in the salesforce.properties file.");		
	}
	
	public String getGovernanceAndChangeConsultantID(){
		String driverPath = properties.getProperty("GovernanceAndChangeConsultantID");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Governance and Change consultant not specified in the salesforce.properties file.");		
	}
	
	public String getGovernanceAndChangeConsultantPwd(){
		String driverPath = properties.getProperty("GovernanceAndChangeConsultantPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("Governance and Change consultant password not specified in the salesforce.properties file.");		
	}
	public String getKLKnowledgeSpecialistKlId(){
		String driverPath = properties.getProperty("KLKnowledgeSpecialistKl");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("KL Knowledge Specialist not specified in the salesforce.properties file.");		
	}
	
	public String getKLKnowledgeSpecialistPwd(){
		String driverPath = properties.getProperty("KLKnowledgeSpecialistPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("KL Knowledge Specialist password not specified in the salesforce.properties file.");		
	}
	public String getUKKnowledgeSpecialistBudapestId(){
		String driverPath = properties.getProperty("UKKnowledgeSpecialistBudapest");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Knowledge Specialist Budapest not specified in the salesforce.properties file.");		
	}
	
	public String getUKKnowledgeSpecialistBudapestPwd(){
		String driverPath = properties.getProperty("UKKnowledgeSpecialistBudapestPwd");
		if(driverPath!= null) return driverPath;
		else throw new RuntimeException("UK Knowledge Specialist Budapest password not specified in the salesforce.properties file.");		
	}
}
