package WeatherAPI.com.bdd;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features="/src/test/resource/Features/ApiTestingWeather.feature",
plugin = {"pretty", "html:target/cucumber-reports"},
//glue = {"src/main/java/WeatherAPI/com/bdd/WeatherStepDefinitions.java"},
monochrome = true)
public class TestRunner {

	

}