package WeatherAPI.com.bdd;



import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

import java.net.MalformedURLException;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import static org.assertj.core.api.Assertions.assertThat;



/****************************************************************************************************************
 * Class NAME: StepDefinationsWeather
 * CREATED BY: Abhilash
 * DATE CREATED: 4-Feb-2019
 * DESCRIPTION: This Class implements the step definations For  the annotations @Given , @When, @hen 
 * PRE_REQUISITES: Feature file should be created
 * PARAMETERS: 
 * 
 * REVIEWED BY & Date:
 * MODIFICATION LOG:
 * ID                            DATE                       INITIALS                   MODIFICATIONS
 ****************************************************************************************************************/

public class StepDefinationsWeather {

	public List<String>CityNames;
	Response validateResponse;
	OpenMapWeatherAPI objweatherAPI = new OpenMapWeatherAPI();
	
	@Given("^I navigate to  Weather API$")
	public void i_navigate_to_Weather_API() throws Throwable {
		int StatusCode= CommonFunctions.ReturnStatusCode("");
		Assert.assertEquals(200, StatusCode);	   
	}

	@When("^I verify weather by \"(.*?)\" for open weather map API$")
	public void i_verify_weather_by_for_open_weather_map_API(String City) throws Throwable {
	
		validateResponse = objweatherAPI.getWeatherAPI(City);
	}

	@Then("^I validate the response which returned from API  \"(.*?)\"$")
	public void i_validate_the_response_which_returned_from_API(String responsecode) throws Throwable {

		int StatusCode= validateResponse.getStatusCode();
		Assert.assertEquals(responsecode, StatusCode);
	}

	@When("^Validate the response from weathermap API is \"(.*?)\"$")
	public void validate_the_response_from_weathermap_API_is(String responsecode) throws Throwable {
	 
		int StatusCode= validateResponse.getStatusCode();
		Assert.assertEquals(responsecode, StatusCode);	
	}

	@Then("^I verify the weather data main details \"(.*?)\" and \"(.*?)\"$")
	public void i_verify_the_weather_data_main_details_and(String fieldname, String fieldvalue) throws Throwable {
	 
		ValidatableResponse value = validateResponse.then();
		JsonPath jsonresponse = value.statusCode(200).extract().jsonPath();

		if(StringUtils.isNumeric(fieldvalue)){

			assertThat(jsonresponse.getInt(fieldname)).isEqualTo(Integer.parseInt(fieldvalue));
		} else{

			assertThat(jsonresponse.getString(fieldname)).isEqualTo(fieldvalue);
		}

	}
	

	
}