package com.infosys.salesforce.runner;

import java.io.File;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.meta.Exhaustive;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.java.Before;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
/*@ExtendedCucumberOptions(jsonReport = "target/cucumber.json", retryCount = 0, detailedReport = true,
detailedAggregatedReport = true, overviewReport = true, outputFolder = "target")
		// coverageReport = true,
		// jsonUsageReport = "target/cucumber-usage.json",
		//usageReport = false, toPDF = false, excludeCoverageTags = { "@flaky" }, includeCoverageTags = {
				//"@passed" },
*/
@CucumberOptions(features = { "src/test/resources/features" }, glue = { "stepDefinitions" }, tags = { "@epic47" },
		// plugin= {"html:target/cucumber-html-report"}
		monochrome = true, plugin = { "html:target/cucumber-html-report", "json:target/cucumber-json-report.json" })
public class RunAllTest {

	public static String formattedDate = null;

	@BeforeClass
	public static void scenarioScreeshot()

	{
		Date date = new Date();
		try {
			DateFormat format = new SimpleDateFormat("dd-MMM-hh.mm.ss");
			formattedDate = format.format(date);
			boolean folderCreated = new File("./Screenshots/" + formattedDate).mkdir();
		} catch (Exception e) {
			System.out.println("Error while creating");
			e.printStackTrace();
		}

		/*
		 * if (folderCreated) { DateFormat format = new
		 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); formattedDate =
		 * format.format(date); boolean folderCreated = new File("./Screenshots/" +
		 * formattedDate).mkdir(); System.out.println("Folder is created"); } else {
		 * System.out.println("Error while creating"); }
		 */

	}
}
