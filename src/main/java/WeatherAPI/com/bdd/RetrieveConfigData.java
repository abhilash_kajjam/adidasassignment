package WeatherAPI.com.bdd;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/*
 * Author :Abhilash
 * Description: Method to get configuration parameters
 * Input:Configuration Key
 * Output:Configuration value
 * Date:02/04/2019
 */

public class RetrieveConfigData {
	//Declare result
	String Result;
	//InputStream Object
	InputStream Inputstream;

	public String GetConfigProperties(String Val)throws IOException
	{
		try{
			Properties properties = new Properties();
			String Filename="Config.properties";
			Inputstream=getClass().getClassLoader().getResourceAsStream(Filename);
			if (Inputstream!=null){
				properties.load(Inputstream);
			}
			else {
				throw new FileNotFoundException("Config file not found"+Filename);
			}
			Result= properties.getProperty(Val);
		}
		catch(Exception e){
			System.out.println("Exception Message"+e);
		}
		return Result;
	}

}
