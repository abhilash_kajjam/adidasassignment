package WeatherAPI.com.bdd;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import cucumber.api.DataTable;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CommonFunctions {

	/**
	 * Description: Method to get response from Base URI
	 * @return httprequest
	 * @throws Throwable
	 */
	public static RequestSpecification SetProxyAndAccessURI() throws Throwable{
		RetrieveConfigData properties = new RetrieveConfigData();
		RestAssured.proxy(properties.GetConfigProperties("Proxy"));
		RestAssured.baseURI=properties.GetConfigProperties("BaseURI");
		RequestSpecification httprequest=RestAssured.given();
		return httprequest;
	}
	/**
	 * @param APIVal
	 * @return
	 * @throws Throwable
	 */
	public static int ReturnStatusCode(String APIVal) throws Throwable{
		int Statuscode;
		Response response= SetProxyAndAccessURI().get(APIVal);
		Statuscode=response.getStatusCode();
		return Statuscode;
	}
	public static String ReturnResponse(String APIVal) throws Throwable{
		String ResponseBody;
		Response response= SetProxyAndAccessURI().get(APIVal);
		ResponseBody=response.getBody().asString();
		return ResponseBody;
	}

	/**
	 * @param DataTable City
	 * @return CityNames
	 * @throws Throwable
	 */
	public static  List<String> ReturnDataTableValues(DataTable City) throws Throwable{
		List<List<String>>data= City.raw();
		List<String>dataval = new ArrayList<String>();
		int size = data.size();
		for (int row = 1; row < size; row++){
			for (int column = 0; column < size; column++){
				String CityURL=data.get(0).get(column);
				dataval.add(CityURL);
			}
		}
		return dataval;
	}
	
	
	/****************************************************************************************************************
	 * FUNCTION NAME: isElementExist
	 * CREATED BY: Abhilash 
	 * DATE CREATED: 05-Feb-2019
	 * DESCRIPTION: This Function checks if the element exists or not
	 * PRE_REQUISITES: NA
	 * PARAMETERS: 1. WebDriver instance 2. WebElement object which needs to be checked
	 * RETURN TYPE: webDriver
	 * RETURN VALUE: TRUE/FALSE
	 * EXAMPLE:
	 * REVIEWED BY:
	 * DATE REVIEWED:
	 * MODIFICATION LOG:
	 * ID                            DATE                       INITIALS                   MODIFICATIONS
	 ****************************************************************************************************************/
	public boolean isElemenExist(WebDriver driver,WebElement elementToCheck)
	{
		try
		{
			WebDriverWait myDynamicWaitElement = new WebDriverWait(driver, 30);
			myDynamicWaitElement.until(ExpectedConditions.visibilityOf(elementToCheck));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(new Function<WebDriver,Boolean>()
			{
				public Boolean apply(WebDriver driver)
				{
					//System.out.println("current window state: "+String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
					return String
							.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
							.equals("complete");
				}
			});
			if(elementToCheck.isDisplayed())
				return true;

		}
		catch(Exception e)
		{
			System.out.println("Exception occured while checking if element exists or not. Element is "+elementToCheck+" Exception is "+e.getMessage());
			return false;
		}
		return false;
}
}
