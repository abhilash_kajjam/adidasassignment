@verifySamplePage
Feature: As a user I want to create a new article

@adidas
Scenario Outline: Verify Sample page enter the comment successfully
Given user navigates to "<browser>" and opens "<demoqa>" site application  
And Navigate to Sample page
When the user enters invalid details "<comment>""<name>""<emailid>" and validate the error message
And navigate to back page
Then  user enters valid details "<comment>""<name>""<emailidvalid>" and validate the success message
And close the browser
Examples:
|browser|demoqa|comment|name|emailid|emailidvalid|
|chrome|adidas_assignment_Url|Entering valid comment|Abhilash|abhilash_kajjam|abhi.kajjam@gmail.com|
