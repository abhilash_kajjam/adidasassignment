package WebTestingDemoQa;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;

public class Page {

	
	private Hooks hooks=new Hooks();
	
	private DemoQA_Login_HomePage demoQA_page;
	private WebDriver driver;
	
	
	
public DemoQA_Login_HomePage getDemoQA_Login_HomePage() {
		
		if(demoQA_page == null)
		{
			
			demoQA_page = new DemoQA_Login_HomePage(driver);
			
		}
		
		return demoQA_page;
		
	}
	

public WebDriver getDriver(String browser, String appName) throws MalformedURLException{

    driver = hooks.setDriver(browser , appName);
    return driver;
    
   }

}
