@OpenWeatherAPI
Feature: As a user I want to verify open weather API by City Name

  Scenario Outline: Retrieve current open weather map using API
    Given I navigate to  Weather API
    When I verify weather by "<CityName>" for open weather map API
    Then I validate the response which returned from API  "<Response>"
    Examples:
    | CityName| Response|
    | uk     	| 200    |
    | 222    	| 404    |
    
  Scenario Outline: Validate Response Message for API
    Given I navigate to  Weather API
    When I verify weather by "<CityName>" for open weather map API
    And Validate the response from weathermap API is "200"
    Then I verify the weather data main details "<fieldname>" and "<value>"
    
   Examples:
    | CityName |fieldname | value  |
    | London   | main.pressure | 1003 |
    | London   | main.humidity | 93 |
    | London   | sys.country | GB |
    
    
    
