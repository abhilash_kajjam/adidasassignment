package WebTestingDemoQa;



import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;

import com.infosys.salesforce.runner.TestRunner;

import WeatherAPI.com.bdd.CommonFunctions;
import cucumber.api.Scenario;


public class DemoQA_Login_HomePage {

	public static Scenario ScenarioName = null;
	/***** Case Page web elements *********/

	@FindBy(xpath="//a[text()='Sample Page']")
	private WebElement Sample_link;
	
	@FindBy(xpath="//textarea[@id='comment']")
	private WebElement comment_Txtbox;
	
	@FindBy(xpath="//input[@id='author']")
	private WebElement name_Txtbox;
		
	@FindBy(xpath="//input[@id='email']")
	private WebElement email_Txtbox;
	
	@FindBy(xpath="//input[@id='url']")
	private WebElement website_Txtbox;
	
	@FindBy(xpath="//input[@name='submit']")
	private WebElement postComment_Btn;
	
	@FindBy(xpath="//*[@id='error-page']/p[2]/text()")
	private WebElement error_InvalidEmail_txt;
	

	Page page = new Page();
	String screenShotPath;
	private WebDriver driver;

	int count = 1;
	
	CommonFunctions commFunc = new CommonFunctions();
	
	
	public DemoQA_Login_HomePage(WebDriver driver)
	{
		super();
		this.driver=driver;
		PageFactory.initElements(driver, this);
	
	}
	
	public String captureScreen()throws Exception
	{ count++;
		try
		{
				
				Thread.sleep(2000);
			     File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			     //The below method will save the screen shot in d drive with name "screenshot.png"
			     //timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());  
			     //FileUtils.copyFile(scrFile, new File("./Screenshots/"+scenario.getName()+"/"+timeStamp+".png"));
			     String outputFolder = TestRunner.formattedDate;			     
			     String path = "./Screenshots/"+TestRunner.formattedDate+"/"+StepDefinationWeb.printScenarioName(StepDefinationWeb.ScenarioName)+"_"+Integer.toString(count)+".png";
			     WriteFile(outputFolder);
			     File dest = new File(path);
			     FileUtils.copyFileToDirectory(scrFile, dest);
			     //FileUtils.copyFile(scrFile, new File(screenShotPath));
			
			return screenShotPath;
			
		}/*catch(IOException i)
		{
			System.out.println("IO exception occured while capturing screenshot "+i.getMessage());
			throw new Exception("IO exception occured while capturing screenshot ");
		}*/
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Another exception occured while capturing screenshot");
		}
	}
	
	
	public boolean verify_SamplePageNavigation() {
		
		boolean flag= false;
		try {
			System.out.println("Verifying Sample page");	
			Sample_link.click();
		
			flag= driver.getCurrentUrl().contains("sample-page");
			flag = true;
				}
		catch(Exception e)
		{			
			System.out.println("Error occured while Navigating to Sample Page"+e.getStackTrace());
			e.printStackTrace();
			flag= false;		
		}
		return flag;
	}
		 
	public boolean demoqa_sample_link()
	{		
		boolean flag= false;
		try {
			
				System.out.println("hiii");			
				Thread.sleep(5000);
				if(commFunc.isElemenExist(driver, Sample_link))
				{
					System.out.println("Smaple link is present in the demo QA page");				
					flag= true;					
				}				
				else
				{
					System.out.println("Smaple link is not present in the page");
					flag= false;				
				}		
		}
		catch(Exception e)
		{
						System.out.println("Smaple link is not present in the page"+e.getStackTrace());
			e.printStackTrace();
			flag= false;		
		}
		System.out.println(flag);
		return flag;
	}

	public boolean navigateBack() {
		 boolean flag= false;
			try {
				System.out.println("Verifying the back");	
				driver.navigate().back();
				
				if(commFunc.isElemenExist(driver, Sample_link)){
					System.out.println("User is on the Sample page");
				}
				else{
					System.out.println("User is still on the error message");
					
				}
			}
				catch(Exception e)
				{
					System.out.println("UI elements not present in the page"+e.getStackTrace());
					e.printStackTrace();
					flag= false;
				}
				return flag;
			}
			
	public boolean submitTextFieldsValidateMsg(String desc, String name,String email) {
		 boolean flag= false;
			try {
				System.out.println("Verifying the failure message");					
				
				if((commFunc.isElemenExist(driver, comment_Txtbox)) && (commFunc.isElemenExist(driver, name_Txtbox)) && (commFunc.isElemenExist(driver, email_Txtbox)))
				{
					
					System.out.println("UI Elements  Comment, Name and Email Textbox exist");	
					postComment_Btn.click();
				if((driver.getTitle().contains("500 Error")) || ((driver.getTitle().contains("Comment Submission Failure")))||(error_InvalidEmail_txt.getText().equalsIgnoreCase("please enter a valid email address"))) {
					System.out.println("Error on the page,Given invalid inputs to submit");
				}
					
					flag= true;					
				}				
				else
				{
					System.out.println(" "+error_InvalidEmail_txt.getText());
					flag= false;				
				}		
			}
			catch(Exception e)
			{
				System.out.println("UI elements not present in the page"+e.getStackTrace());
				e.printStackTrace();
				flag= false;
			}
			return flag;
	 }
			public boolean browserClose() {
				 boolean flag= false;
				  {
						System.out.println("Closing the browser");
						driver.quit();
				  }
				return flag;
			}
	
	 public boolean submitTextFields(String desc, String name,String email) {
		 boolean flag= false;
			try {
				System.out.println("Verifying the failure message");					
				
				if((commFunc.isElemenExist(driver, comment_Txtbox)) && (commFunc.isElemenExist(driver, name_Txtbox)) && (commFunc.isElemenExist(driver, email_Txtbox)))
				{
					
					System.out.println("UI Elements  Comment, Name and Email Textbox exist");	
					postComment_Btn.click();
				if((driver.getTitle().contains("500 Error")) || ((driver.getTitle().contains("Comment Submission Failure")))||(error_InvalidEmail_txt.getText().equalsIgnoreCase("please enter a valid email address"))) {
					System.out.println("Error on the page,Given invalid inputs to submit");
				}
					
					flag= true;					
				}				
				else
				{
					System.out.println("Smaple link is not present in the page"+error_InvalidEmail_txt.getText());
					flag= false;				
				}		
			}
			catch(Exception e)
			{
				System.out.println("UI elements not present in the page"+e.getStackTrace());
				e.printStackTrace();
				flag= false;
			}
			return flag;
	 }
	
	
	
	
	

private void WriteFile(String outputFolder) {
		// TODO Auto-generated method stub
		
	}
	
}
