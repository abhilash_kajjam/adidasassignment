package WeatherAPI.com.bdd;

import io.restassured.response.Response;

public class OpenMapWeatherAPI {

	
	
	RetrieveConfigData properties = new RetrieveConfigData();
	
	public String getAPICityName(String CityName) throws Throwable{
		String WeatherUrl="?q="+CityName+"&appid=b6907d289e10d714a6e88b30761fae22";
		return WeatherUrl;
	}
	public Response getWeatherAPI(String CityName) throws Throwable{
		String URI=getAPICityName(CityName);
		Response validateResponse= CommonFunctions.SetProxyAndAccessURI().get(URI);
		return validateResponse;
	}
	public Response getWeatherAPI(String CityName,String Country,String key) throws Throwable{
		String InputType=properties.GetConfigProperties("keyName");
		Response validateResponse= CommonFunctions.SetProxyAndAccessURI().get("/weather"+InputType+CityName+","+Country+"&appid=b6907d289e10d714a6e88b30761fae22");
		return validateResponse;
	}
}
