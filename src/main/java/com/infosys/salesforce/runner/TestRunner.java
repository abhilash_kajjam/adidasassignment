package com.infosys.salesforce.runner;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(ExtendedCucumber.class)

@ExtendedCucumberOptions( jsonReport = "target/cucumber.json", detailedReport = true,
detailedAggregatedReport = true, overviewReport = true,
		 coverageReport = true,
		 jsonUsageReport = "target/cucumber-usage.json",
		overviewChartsReport = true,
		usageReport = false, toPDF = false,outputFolder = "target" )

@CucumberOptions(features = { "src/test/resources/features" }, glue = { "stepDefinitions" }, 

tags= {"@adidas"},
monochrome = true, plugin = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber.json","pretty:target/cucumber-pretty.txt", 
"usage:target/cucumber-usage.json","junit:target/cucumber-results.xml" })

public class TestRunner {

	public static String formattedDate = null;

	@BeforeClass
	public static void scenarioScreeshot()

	{
		Date date = new Date();
		try {
			
			DateFormat format = new SimpleDateFormat("dd-MMM-hh.mm.ss");
			formattedDate = format.format(date);
			boolean folderCreated = new File("./Screenshots/" + formattedDate).mkdir();
			
		} catch (Exception e) {
			System.out.println("Error while creating");
			e.printStackTrace();
		}

	}
	
}
