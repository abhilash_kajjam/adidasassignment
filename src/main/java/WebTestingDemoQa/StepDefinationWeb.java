package WebTestingDemoQa;

import java.util.Collection;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinationWeb {
	
	private static String screenShotPath =null;
	public static Scenario ScenarioName = null;
	public static String CucumberScenario = null; 
WebDriver driver;
	
	private Page page;

	
	@Before
    public static String printScenarioName(Scenario scenario) {
    	
        System.out.println("Run into Before Hook: printScenarioName");
        System.out.println("Print Scenario name in Before Hook: " + scenario.getName());
        Collection<String> scenarioTag = scenario.getSourceTagNames();
        ScenarioName= scenario;
        scenarioTag.forEach( tag ->{
        	if(tag.startsWith("@")) {
        		CucumberScenario = tag.substring(1);
        	}
        });
        return CucumberScenario;
    }

@Given("^user navigates to \"(.*?)\" and opens \"(.*?)\" site application$")
public void user_navigates_to_and_opens_site_application(String browser, String app) throws Throwable {
		
	driver=page.getDriver(browser,app);	
	if(app.equalsIgnoreCase("adidas_assignment_Url")) {
							
			Boolean isSAmpleLinkPresent=page.getDemoQA_Login_HomePage().demoqa_sample_link();			
			Assert.assertTrue("Sample link is prestnt", isSAmpleLinkPresent);
			screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
	}
		else
		{
					
			Assert.fail("Home page to the demo site not navigated failed");			
		}   
}


@Given("^Navigate to Sample page$")
public void navigate_to_Sample_page() throws Throwable {
	
		boolean pageverify=page.getDemoQA_Login_HomePage().verify_SamplePageNavigation();
		if( pageverify) {
			Assert.assertTrue("User is able to navigate to  the Sample page Page : ", pageverify);
			screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
		}
		else{
			Assert.fail("Sample page not navigated");	
			screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
		}
}

@When("^the user enters invalid details \"(.*?)\"\"(.*?)\"\"(.*?)\" and validate the error message$")
public void the_user_enters_invalid_details_and_validate_the_error_message(String desc, String name, String email) throws Throwable {
   
	boolean pageElement =page.getDemoQA_Login_HomePage().submitTextFields(desc, name, email);
			if( pageElement) {
				Assert.assertTrue("Text  Elements in Sample page identified : ", pageElement);
				screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
			}
			else{
				Assert.fail("Text elements not present");	
				screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
			}
}

@When("^navigate to back page$")
public void navigate_to_back_page() throws Throwable {
    
	boolean pageEle =page.getDemoQA_Login_HomePage().navigateBack();
	if(pageEle) {
		Assert.assertTrue("User navigated to Sample page : ", pageEle);
		screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
	}
	else{
		Assert.fail("User not navigated to the Sample Page");	
		screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
	}
}

@Then("^user enters valid details \"(.*?)\"\"(.*?)\"\"(.*?)\" and validate the success message$")
public void user_enters_valid_details_and_validate_the_success_message(String arg1, String arg2, String arg3) throws Throwable {
    
    
}
@Then("^close the browser$")
public void close_the_browser() throws Throwable {
   
	page.getDemoQA_Login_HomePage().browserClose();
	screenShotPath = page.getDemoQA_Login_HomePage().captureScreen();
	}

}
